<?php

/**
 * This class just does some calculations on dates
 *
 * @author     Richard Klement
 * @version    1.0
 * ...
 */


class dates
{

    //functions

    /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    /* check if a date is before the present moment and return true if it is */
    /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    function checkDate($date){
        if( strtotime($date) < strtotime('now') ) {
        return true;
        }
        else{
            return false;
        }

}

    /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    /* Find the difference between two dates and return the difference in days*/
    /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

function differenceDate($date){
       $date1 = strtotime($date);
       $dateNow  = strtotime('now');

       if($date1 > $dateNow){
    $diff = abs($date1 - $dateNow);
}
       else{
           $diff = abs($dateNow - $date1);
       }

       $diff = $diff / (60 * 60 *24);

       return $diff;
}

    /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    /* Return the date of the next wednesday from a given date*/
    /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

function nextWednesday($date){


    $next_wed = date('Y-m-d', strtotime("next wednesday", strtotime($date)));

    return $next_wed;
    }

    /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    /* Compare two dates and return 1 if the first is larger than the second else return -1 */
    /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    function compareDates($date1, $date2){
        if (strtotime($date1) > strtotime($date2))
            return 1;
        else if (strtotime($date1) < strtotime($date2))
            return -1;
        else
            return 0;
    }

    /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    /* Performs a usort on an array of dates and returns the sorted array */
    /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

function sortDates($dates){


        usort($dates, [$this,'compareDates']);


        return $dates;
}

    /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    /* WP_Query returning 100 posts of type project */
    /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

function getProjects(){
    $args = array(
        'post_type'      => 'project',
        'posts_per_page' => '100'
    );
    
    return new WP_Query($args);

}

}

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
/* tests */
/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

$dates = new dates();

echo '<p>'.$dates->checkDate('10 july 2020').'</p>';

echo '<p>'.$dates->differenceDate('10 july 2020').'</p>';

echo '<p>'.$dates->nextWednesday('2021-01-12').'</p>';

$unsortedDates = array('2020-06-10', '2011-06-11', '2021-01-05');

print_r($dates->sortDates($unsortedDates));

